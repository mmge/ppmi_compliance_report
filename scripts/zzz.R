# The date/time the data was compiled
build_date <- get_compliance_data('build_date')
# The latest date included in this report.

end_date <- as.Date(get_compliance_data('end_date'))
BdY_b      <- format(build_date, "%B %d, %Y")
BdY_e      <- format(end_date, "%B %d, %Y")
HM_b       <- format(build_date, "%H:%M")
BY_e       <- format(end_date, '%B %Y')
B_e        <- format(end_date, '%B')
Y_e        <- format(end_date, '%Y')
Y.m_e      <- format(end_date, '%Y.%m')
tp_e       <- paste0("Month.", format(end_date, '%Y.%m'))



