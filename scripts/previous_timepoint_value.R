# Get the value of a specific field at the previous timepoint
previous_timepoint_value <- Vectorize(function(cno, timepoint, group_scores, field) {

  tp <- previous_timepoint(timepoint)

  tpd <- as.list(group_scores[(group_scores$TIMEPOINT_LABEL == tp & group_scores$CNO == cno), ])

  if(missing(field)) {
    return(tpd)
  } else {
    return(tpd[[field]])
  }

}, vectorize.args = c("cno", "timepoint"))
