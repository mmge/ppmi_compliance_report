## `r report_label` Compliance Heatmap

The heatmap below shows quarterly `r tolower(report_label)` compliance scores
for all current sites in the PPMI network. Sites are listed along the y-axis and
have been ordered from highest volume at the top to lowest volume at the bottom.

```{r echo=FALSE, fig.align='center'}

compliance_heatmap(
  score_type = report_section,
  start_year = start_year
)

```
